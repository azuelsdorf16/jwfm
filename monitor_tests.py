import unittest
from monitor import is_hostile_country, ip_geolocation_lookup

class MonitorTest(unittest.TestCase):
    def test_ip_geolocation_lookup(self):
        #36.51.254.234 is the IP address (as of 18 June 2019) of the Chicom
        #social media network weibo.com
        lookup = ip_geolocation_lookup("36.51.254.234")

        self.assertEqual(lookup.get("region", ""), "22")
        self.assertEqual(lookup.get("city", ""), "Beijing")
        self.assertEqual(lookup.get("country", ""), "CN")
        self.assertEqual(lookup.get("location_str", ""), "Beijing, 22, CN")
        
        #104.117.11.176 is the IP address (as of 18 June 2019) of the official
        #CIA website, cia.gov . The city and state may have changed but the
        #country should be the U.S.
        lookup = ip_geolocation_lookup("104.117.11.176")

        self.assertEqual(lookup.get("country", ""), "US")

    def test_is_hostile_country(self):
        #36.51.254.234 is the IP address (as of 18 June 2019) of the Chicom
        #social media network weibo.com
        lookup = ip_geolocation_lookup("36.51.254.234")

        is_hostile = is_hostile_country(lookup)

        self.assertTrue(is_hostile)

        #104.117.11.176 is the IP address (as of 18 June 2019) of the official
        #CIA website, cia.gov . The city and state may have changed but the
        #country should be the U.S.
        lookup = ip_geolocation_lookup("104.117.11.176")

        is_hostile = is_hostile_country(lookup)

        self.assertFalse(is_hostile)

if __name__ == "__main__":
    unittest.main()
