import socket
from winpcapy import WinPcapUtils
import dpkt
import psutil
import sys
import pygeoip
import os
from PyQt5.QtWidgets import QMessageBox, QPushButton
import platform

HOSTILE_COUNTRIES = {"CN" : "China", "RU" : "Russia", "VZ" : "Venezuela", "CU" : "Cuba", "IR" : "Iran", "NK" : "North Korea"}

def kill_process(info):
    if platform.system() == "Windows":
        os.system("taskkill /f /im \"{}\"".format(info.name()))
    else:
        os.system("kill -9 \"{}\"".format(info.pid))

def create_alert(processInfo, senderAddress,
        destinationAddress, countryCode):
    msg = QMessageBox()

    msg.setIcon(QMessageBox.Information)

    msg.setText("ALERT: Connection to {} detected by process {}. Do you want to end this process?".format(HOSTILE_COUNTRIES[countryCode], processInfo.name()))

    msg.addButton(QPushButton("Yes"), QMessageBox.YesRole)
    msg.addButton(QPushButton("No"), QMessageBox.NoRole)
    msg.addButton(QPushButton("View Detailed Process Info"), QMessageBox.RejectRole)

    choice = msg.exec_()

    #0 is code when the "OK" button is clicked.
    while choice != 1 and choice != 0:
        msg.setText("Process name: \"{}\". Location: \"{}\". Sender IP address: {} . Destination IP address: {}, Country code: \"{}\" ({}).".format(processInfo.name(), processInfo.exe(), senderAddress, destinationAddress, countryCode, HOSTILE_COUNTRIES[countryCode]))

        choice = msg.exec_()

    if choice == 0:
        kill_process(processInfo)
        return True
    elif choice == 1:
        return False
    else:
        print("WARNING: Invalid choice detected: \"{}\"".format(choice))
        return False

def is_hostile_country(lookup):
    lookup_country = lookup.get("country", None)

    for code, country in HOSTILE_COUNTRIES.items():
        if code == lookup_country:
            return True

    return False

def ip_geolocation_lookup(ip_addr, database_file_name=os.path.join("GeoLiteCity", "GeoLiteCity.dat")):
    lookup = dict()

    try:
        database = pygeoip.GeoIP(database_file_name)

        match = database.record_by_name(ip_addr)

        if match is not None:
            lookup["city"] = match.get('city', None)
            lookup["region"] = match.get('region_code', None)
            lookup["country"] = match.get('country_code', None)
        else:
            lookup["city"] = None
            lookup["region"] = None
            lookup["country"] = None
    except socket.gaierror:
        lookup["city"] = None
        lookup["region"] = None
        lookup["country"] = None

    lookup["location_str"] = "{0}, {1}, {2}".format(lookup['city'],
    lookup['region'], lookup['country'])

    return lookup

def monitor():
    #Get info on currently-running processes. Get this first because process of
    #interest could terminate quickly after sending packet.
    processInfo = [_ for _ in psutil.process_iter()]

    #Iterate through processes and find what connections they are maintaining.
    for info in processInfo:
        try:
            for connection in info.connections(kind="inet"):
                senderAddress = None
                if len(connection.laddr) == 2 and connection.laddr[0] != "::":
                    senderAddress = "{0}:{1}".format(
connection.laddr[0], connection.laddr[1])

                destinationAddress = None
                if len(connection.raddr) == 2 and connection.raddr[0] != "::":
                    destinationAddress = "{0}:{1}".format(
connection.raddr[0], connection.raddr[1])

                if destinationAddress is not None and senderAddress is not None:
                    lookup_info = ip_geolocation_lookup(connection.raddr[0])
                    #lookup_info = ip_geolocation_lookup("36.51.254.234")

                    is_hostile = is_hostile_country(lookup_info)

                    if is_hostile:
                        print("WARNING: Hostile process detected: \"{}\"".format(info.name()))
                        process_killed = create_alert(info, senderAddress,
                                destinationAddress, lookup_info["country"])

                        #Do not continue evaluating the process's connections
                        #if we killed the process.
                        if process_killed:
                            break

        except psutil.AccessDenied:
            sys.stderr.write("Couldn't get info on process \"{}\" due to permissions error\n".format(
                info.name()))
        except psutil.NoSuchProcess:
            pass

def main():
    try:
        monitor()
    except KeyboardInterrupt:
        sys.stderr.write("Received keyboard interrupt. Exiting now...\n")

if __name__ == "__main__":
    main()
