from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QPushButton
from PyQt5.QtGui import QFont
import sys
from monitor import monitor
from datetime import datetime
import time

class JWFMMainGui(QWidget):
    def __init__(self, app):
        super().__init__()

        print("Started monitoring at UTC time {}".format(datetime.utcnow()))

        while True:
            monitor()
            time.sleep(5)
        
        print("Ended monitoring at UTC time {}".format(datetime.utcnow()))

        app.quit()

def main():
    app = QApplication(sys.argv)
    main_gui = JWFMMainGui(app)
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
