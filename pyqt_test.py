from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QPushButton
from PyQt5.QtGui import QFont
import sys

class JWFMMainGui(QWidget):
    def __init__(self):
        super().__init__()

        self.button_clicks = 0
        self.bigShinyButton = None
        self.bigShinyLabel = None

        self.populateGUI()

    def increment_clicks_handler(self):
        self.button_clicks += 1
        self.bigShinyLabel.setText("<i>Button clicks: {0}<i>".format(self.button_clicks))
        self.bigShinyLabel.resize(self.bigShinyLabel.sizeHint())

    def populateGUI(self):
        self.bigShinyLabel = QLabel(self)
        self.bigShinyLabel.setFont(QFont('SansSerif', 10))
        self.bigShinyLabel.setText("<i>Button clicks: {0}<i>".format(self.button_clicks))
        self.setGeometry(100,100,200,50)
        self.bigShinyLabel.move(50,20)
   
        #Create button
        self.bigShinyButton = QPushButton('Click Me', self)
        self.bigShinyButton.clicked.connect(self.increment_clicks_handler)
        self.bigShinyButton.resize(self.bigShinyButton.sizeHint())
        self.bigShinyButton.move(50, 50)

        self.setGeometry(300, 300, 250, 150);
        self.setWindowTitle('Button Click Demo')
        self.show()

def main():
    app = QApplication(sys.argv)
    main_gui = JWFMMainGui()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
